# Task list for Mimacom job application

This repository contains a Java+Spring backend application that exposes an API 
rest to manage a task list (tasks with CRUD operations and ability to mark them 
as finalized).

## API Endpoints

All endpoints are secured and return a 500 HTTP Status Code in case of any unexpected exception.

### Tasks

```
http://localhost:8080/tasks
```

#### Get

Returns all tasks of the current user not already finalized and sorted by title.

Optional parameters:
* `pageNo`: integer representing the number of a page to be returned. Default: `0`.
* `pageSize`: integer representing the size of the page to be returned. Default: `10`.
* `sortBy`: string representing field to sort the results. Default: `title`. Allowed values: `title` and `deadline`.

Returns for each task:
* `id`: integer. Unique.
* `title`: string with the title of the task.
* `deadline`: if present [string representation of a date with an offset](https://docs.oracle.com/javase/8/docs/api/java/time/OffsetDateTime.html#parse-java.lang.CharSequence-).
* `finalized`: boolean representing if the task is finalized. In this case, is always `false`.

Example:

```
curl -u username:password localhost:8080/tasks?pageNo=3&pageSize=5&sortBy=deadline
```

#### Post

Creates a new task marked as not finalized and with a unique id.

Mandatory body parameters:
* `title`: string representing the title of the task. If it's blank or empty a 400 status code would be received. 

Optional body parameters:
* `deadline`: [string representation of a date with an offset](https://docs.oracle.com/javase/8/docs/api/java/time/OffsetDateTime.html#parse-java.lang.CharSequence-).

Returns:

* `id`: long. Unique.
* `title`: string with the title of the task.
* `deadline`: if present [string representation of a date with an offset](https://docs.oracle.com/javase/8/docs/api/java/time/OffsetDateTime.html#parse-java.lang.CharSequence-).
* `finalized`: boolean representing if the task is finalized. In this case, is always `false`.

In case of error:
* 400 HTTP Status Code (BAD_REQUEST) if the title is blank or empty or there is no body in the request.

Example:

```
curl -u username:password -H "Content-Type: application/json" -d '{"title":"test with deadline", "deadline":"2020-05-26T09:34:30+01:00"}' localhost:8080/tasks
```

#### Put

Edits a task.

Path variable:

```
http://localhost:8080/tasks/{id}
```

* id: mandatory. Long representing the id of the task to be edited.

Body parameters (at least one has to be present):
* `title`: string with the title of the task.
* `deadline`: [string representation of a date with an offset](https://docs.oracle.com/javase/8/docs/api/java/time/OffsetDateTime.html#parse-java.lang.CharSequence-).
* `finalize`: boolean representing if the task is finalized.

Returns:
* `id`: integer. Unique.
* `title`: string with the title of the task.
* `deadline`: if present [string representation of a date with an offset](https://docs.oracle.com/javase/8/docs/api/java/time/OffsetDateTime.html#parse-java.lang.CharSequence-).
* `finalized`: boolean representing if the task is finalized.

In case of error:
* 400 HTTP Status Code (BAD_REQUEST) in the following cases:
    * No body parameters at all in the request or no body.
    * There's no task with the provided path variable `id`.
    * `deadline` is not a correct representation of a date with an offset.

Examples:

```
curl -u username:password -H "Content-Type: application/json" -X PUT -d '{"title":"Test", "deadline":"2020-05-26T09:34:30+01:00"}' localhost:8080/tasks/1
curl -u username:password -H "Content-Type: application/json" -X PUT -d '{"finalize":"true"}' localhost:8080/tasks/1

```

#### Delete

Deletes a task.

Path variable:

```
http://localhost:8080/tasks/{id}
```

* id: mandatory. Integer representing the id of the task to be deleted.

Example:

```
curl -u username:password -X DELETE localhost:8080/tasks/1
```

### History

```
http://localhost:8080/history
```

#### Get

Returns all tasks of the current user already finalized and sorted by title.

Optional parameters:
* `pageNo`: integer representing the number of a page to be returned. Default: `0`.
* `pageSize`: integer representing the size of the page to be returned. Default: `10`.

Returns for each task:
* `id`: integer. Unique.
* `title`: string with the title of the task.
* `deadline`: if present [string representation of a date with an offset](https://docs.oracle.com/javase/8/docs/api/java/time/OffsetDateTime.html#parse-java.lang.CharSequence-).
* `finalized`: boolean representing if the task is finalized. In this case, is always `true`.

Example:

```
curl -u username:password localhost:8080/history?pageNo=3&pageSize=5
```

## Test strategy

Goals:
* Unit tests should explore all algorithm independent paths.
* Integration tests should explore all dependencies and happy paths of each endpoint.

## Log configuration reloads on-the-fly

Any public method invocation of the application may log its entry parameters, it's return value and its execution time thanks to AOP. You may change its logging level on-the-fly editing its configuration file. Check Installation section for more information.

## Technologies

* Spring boot 2.5.6
  * Spring Web
  * Spring Data JPA
  * Spring Boot DevTools (dev)
  * Spring Security
  * Spring Validation
  * Spring AOP
  * Java Mail Sender
* Java 11
* Maven
* MySQL Connector (prod)
* Apache HTTPClient (tests)
* H2 Database (tests)

## Installation

For a demo with an in-memory database simply clone the repository and execute 

```
./mvnw spring-boot:run
```

Or if you prefer to use docker

```
./mvnw spring-boot:build-image
docker run -it -p8080:8080 tasks:1.0.0.RELEASE

```

If you want to change log configuration on the fly edit `target/classes/logback.xml` file.

### Production environment

#### Database configuration

Install mysql-server and login as root.

Create the MySQL Database:

```
create database tasks;
create user 'admintasks'@'%' identified by 'PASSWORD';
grant select, insert, delete, update on tasks.* to 'admintasks'@'%';
flush privileges;
connect tasks;
CREATE TABLE hibernate_sequence (next_val bigint(20) DEFAULT NULL);
CREATE TABLE users (username varchar(15) NOT NULL, password varchar(255) NOT NULL, email varchar(255) DEFAULT NULL, locale varchar(5) DEFAULT NULL, PRIMARY KEY(username));
CREATE TABLE tasks (id bigint(20) NOT NULL, finalized bit(1) NOT NULL, title varchar(255) NOT NULL, user_username varchar(15) NOT NULL, deadline datetime DEFAULT NULL, PRIMARY KEY(id), KEY FK_user_username (user_username), CONSTRAINT FK_user_username FOREIGN KEY (user_username) REFERENCES users (username));
INSERT INTO hibernate_sequence (next_val) VALUES (1);
```

#### HTTPS Configuration

Obtain an SSL certificate or, alternatively create a self-signed one:

```
keytool -genkey -alias springboottasks -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650
```

Modify `application-prod.properties` with your datasource configuration or create an external file

```
server.port: 443
server.ssl.key-store: keystore.p12
server.ssl.key-store-password: springboot
server.ssl.keyStoreType: PKCS12
server.ssl.keyAlias: tomcat
```
#### Datasource configuration

Modify `application-prod.properties` with your datasource configuration or create an external file

```
spring.jpa.hibernate.ddl-auto=none
spring.jpa.database-platform=org.hibernate.dialect.MariaDBDialect
spring.datasource.url=jdbc:mysql://HOST:PORT/DATABASE?servertime-zone=UTC
spring.datasource.username=admintasks
spring.datasource.password=PASSWORD
```

#### Mail server configuration

Modify `application-prod.properties` with your datasource configuration or create an external file

```
spring.mail.host=smtp.host.com
spring.mail.port=587 
spring.mail.username=test@host.com
spring.mail.password=PASSWORD
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true
```

#### Creation of new users

Use Spring encodepassword tool from [Spring Boot CLI](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html#getting-started-installing-the-cli) to obtain the encrypted password to insert in the database:

```
spring encodepassword PASSWORD
```

#### Run the application

Clone the repository and execute

```
./mvnw -Denv=prod -Dspring.config.location=PATH/TO/FILE/application-prod.properties spring-boot:run
```

If you want to be able to change the log configuration on the fly execute to take into account an external `logback.xml` file. Use the one in the project as a base and remember to activate the `debug=true` option to be able to know when the configuration is reloaded.

```
./mvnw -Denv=prod -Dlogging.config=PATH/TO/FILE.xml -Dspring.config.location=PATH/TO/FILE/application-prod.properties spring-boot:run
```

You can create a Docker image for production

```
./mvnw -Denv=prod spring-boot:build-image
docker run -it -p4430:4430 tasks:1.0.0.RELEASE

```

### Troubleshooting

#### Production: MariaDB: Index column size too large. The maximum column size is 767 bytes

Update `my.cnf` with the following config@EnableAutoConfigurationuration:
```
innodb_default_row_format=dynamic
innodb_file_format=barracuda
innodb_file_per_table=true
innodb_large_prefix=true
```

And then restart the service.

#### Production: Unable to start embedded Tomcat server. Permission denied

You must be root to bind any port under 1024. Execute as superuser or use a port over 1024.

#### Production: curl SSL certificate problem: self-signed certificate

Use option `-k` or `--insecure` in your curl commands.

## TODO

* [X] Add authentication and users.
    * [x] Link tasks with users.
* [X] Add a persistent DB.
* [X] Add ability to lists finalized tasks.
* [X] Add logging to the application.
* [X] Add a docker image.
* [X] Improve unit testing using mocks.
* [X] Add a License.
* [X] Evaluate Spring HATEOAS.
* [X] Add ability to sort and paginate queries.
* [X] Static Code Analysis (Sonarqube, PMD).
* [X] Add "Deadline" field to tasks.
    * [X] Add "email" to users.
    * [X] Add i18n.
    * [X] Add ability to sort by "Deadline".
    * [X] Add scheduled tasks that sends notifications emails to users with non-finalized tasks that are close to their deadline.
* [X] Evaluate CI/CD. 
* [X] Evaluate Spring Vault.