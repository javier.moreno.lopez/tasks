INSERT INTO users (username, password, email, locale)
VALUES ('javi', '{bcrypt}$2a$10$u3luoZO00b.4A6l14OfkD.4uIKBNtOWA2PKx/7TqaPCM4R6FCrGbe', 'prueba@prueba.com', 'es-ES');
INSERT INTO users (username, password, locale)
VALUES ('sandra', '{bcrypt}$2a$10$jukmiRbv1H0zcA8PRIiZQ.myeKAnv7Gj3YToD7Cn0UsCzdKLv9/aS', 'es-ES');