package com.mimacom.es.recruitment.tasks;

import com.mimacom.es.recruitment.tasks.services.UserService;
import com.mimacom.es.recruitment.tasks.web.TasksLocaleResolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.LocaleResolver;

@SpringBootApplication
@EnableScheduling
public class TasksApplication {
	private final UserService userService;

	public TasksApplication(UserService userService) {
		this.userService = userService;
	}

	@Bean
	public LocaleResolver localeResolver() {
		return new TasksLocaleResolver(userService);
	}

	public static void main(String[] args) {
		SpringApplication.run(TasksApplication.class, args);
	}
}
