package com.mimacom.es.recruitment.tasks.validation;

import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotBlankIfPresentValidator implements ConstraintValidator<NotBlankIfPresent, String> {

    @Override
    public void initialize(NotBlankIfPresent text) {
    }

    @Override
    public boolean isValid(String text, ConstraintValidatorContext cxt) {
        return text == null || StringUtils.hasText(text);
    }
}