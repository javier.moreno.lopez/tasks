package com.mimacom.es.recruitment.tasks.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Stores a task that has to have a title and may be finalized.
 */
@Entity
@Table(name = "tasks")
public class Task implements Serializable {
	private static final long serialVersionUID = -5614459803413016041L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne
	@JsonIgnore
	@NotNull
	private User user;

	@Size(min = 1, max = 255, message = "validation.size.task.title")
	@NotBlank
	private String title;

	private OffsetDateTime deadline;

	private boolean finalized;

	// For the sake of JPA
	protected Task() {
		super();
	}

	/**
	 * Creates a new Task marked as not finalized.
	 *
	 * @param user  must not be null
	 * @param title must not be empty, max length 255
	 * @throws IllegalArgumentException if title is <code>null</code> or empty.
	 */
	public Task(User user, String title) {
		this(user, title, null);
	}

	/**
	 * Creates a new Task marked as not finalized.
	 *
	 * @param user     must not be null
	 * @param title    must not be empty, max length 255
	 * @throws IllegalArgumentException if title is <code>null</code> or empty.
	 */
	public Task(User user, String title, OffsetDateTime deadline) {
		this();

		this.user = user;
		this.title = title;
		this.deadline = deadline;
		finalized = false;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	/**
	 * @param title must not be empty, max length 255
	 * @throws IllegalArgumentException if title is <code>null</code> or empty.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isFinalized() {
		return finalized;
	}

	public void setFinalized(boolean finalized) {
		this.finalized = finalized;
	}

	public OffsetDateTime getDeadline() {
		return deadline;
	}

	public void setDeadline(OffsetDateTime deadline) {
		this.deadline = deadline;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", user=" + user + ", title=" + title + ", deadline=" + deadline + ", finalized="
				+ finalized + "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Task task = (Task) o;
		return Objects.equals(id, task.id) && user.equals(task.user) && title.equals(task.title);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, user, title);
	}
}
