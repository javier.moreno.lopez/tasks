package com.mimacom.es.recruitment.tasks.services;

import com.mimacom.es.recruitment.tasks.domain.User;
import com.mimacom.es.recruitment.tasks.persistence.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	private final UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * @return the corresponding stored user of the principal.
	 */
	public User getUser() {
		return userRepository.findById(SecurityContextHolder.getContext().getAuthentication().getName())
				.orElseThrow(IllegalStateException::new); //Unreachable. Spring security won't allow a call with a non-recognized user
	}
}
