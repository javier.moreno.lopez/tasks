package com.mimacom.es.recruitment.tasks.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Handles all the uncaught exceptions and returns a ResponseStatusException.
 */
@ControllerAdvice
class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	private final MessageSource messageSource;

	public GlobalExceptionHandler(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@ExceptionHandler(value = {Exception.class, RuntimeException.class})
	protected ResponseEntity<Object> handleInternal(RuntimeException ex, WebRequest request) {
		LOGGER.error("Exception caught by global handler", ex);
		String bodyOfResponse = "An internal error was thrown";
		return handleExceptionInternal(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage()),
				bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	@ExceptionHandler(value = EntityNotFoundException.class)
	protected ResponseEntity<Object> handleNotFound(EntityNotFoundException ex, WebRequest request) {
		return handleExceptionInternal(new ResponseStatusException(
						HttpStatus.NOT_FOUND,
						ex.getMessage()),
				null,
				new HttpHeaders(),
				HttpStatus.NOT_FOUND,
				request
		);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		final Locale locale = LocaleContextHolder.getLocale();
		String message = ex.getBindingResult()
				.getFieldErrors()
				.stream()
				.filter(e -> e.getDefaultMessage() != null)
				.map(e -> String.format("'%s' %s",
						e.getField(),
						(e.getDefaultMessage().matches("^\\{.+\\}$"))
								? messageSource.getMessage(e.getDefaultMessage().substring(1, e.getDefaultMessage().length() - 1), null, locale)
								: e.getDefaultMessage()
				))
				.collect(Collectors.joining(". "));
		return handleExceptionInternal(new ResponseStatusException(HttpStatus.BAD_REQUEST, message), message,
				new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	@ExceptionHandler(value = ResponseStatusException.class)
	protected ResponseEntity<Object> handleResponseStatus(ResponseStatusException ex, WebRequest request) {
		return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), ex.getStatus(), request);
	}
}
