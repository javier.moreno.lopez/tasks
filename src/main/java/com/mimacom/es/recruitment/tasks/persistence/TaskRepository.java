package com.mimacom.es.recruitment.tasks.persistence;

import com.mimacom.es.recruitment.tasks.domain.Task;
import com.mimacom.es.recruitment.tasks.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {

	Page<Task> findByFinalizedAndUser(boolean finalized, User user, Pageable pageable);

	Optional<Task> findByIdAndUser(long id, User user);

	void deleteByIdAndUser(long id, User user);

	List<Task> findAllByUserAndFinalizedIsFalseAndDeadlineLessThan(User user, OffsetDateTime deadline);
}
