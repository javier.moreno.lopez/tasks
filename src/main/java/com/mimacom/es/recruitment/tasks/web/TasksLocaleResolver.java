package com.mimacom.es.recruitment.tasks.web;

import com.mimacom.es.recruitment.tasks.domain.User;
import com.mimacom.es.recruitment.tasks.services.UserService;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Gives priority to request header, if not present defaults to stored DB value and if not present then it uses default
 * system locale.
 */
@Component
public class TasksLocaleResolver extends AcceptHeaderLocaleResolver {

	private final UserService userService;

	public TasksLocaleResolver(UserService userService) {
		this.userService = userService;
	}

	@Override
	public Locale resolveLocale(HttpServletRequest request) {
		String headerLocale = request.getHeader("Accept-Language");
		if (!(null == headerLocale || headerLocale.trim().isEmpty())) {
			return Locale.forLanguageTag(request.getHeader("Accept-Language"));
		}

		User user = userService.getUser();
		if (null == user || null == user.getLocale() || user.getLocale().trim().isEmpty()) {
			return getDefaultLocale();
		}

		return Locale.forLanguageTag(user.getLocale());
	}
}
