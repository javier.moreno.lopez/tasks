package com.mimacom.es.recruitment.tasks.web;

import java.time.OffsetDateTime;

public class UpdateRequest extends CreateRequest {

    private final Boolean finalized;

    public UpdateRequest(String title, OffsetDateTime deadline, Boolean finalized) {
        super(title, deadline);
        this.finalized = finalized;
    }

    public Boolean getFinalized() {
        return finalized;
    }

    @Override
    public String toString() {
        return "UpdateRequest{" +
                "title='" + title + '\'' +
                ", deadline=" + deadline +
                ", finalized=" + finalized +
                '}';
    }
}
