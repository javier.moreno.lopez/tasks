package com.mimacom.es.recruitment.tasks.web;

import java.io.Serializable;
import java.time.OffsetDateTime;

public class TaskResponse implements Serializable {

    private static final long serialVersionUID = 7937607296749689122L;

    private final long id;
    private final String title;
    private final OffsetDateTime deadline;
    private final boolean finalized;

    public TaskResponse(long id, String title, OffsetDateTime deadline, boolean finalized) {
        this.id = id;
        this.title = title;
        this.deadline = deadline;
        this.finalized = finalized;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public OffsetDateTime getDeadline() {
        return deadline;
    }

    public boolean isFinalized() {
        return finalized;
    }
}
