package com.mimacom.es.recruitment.tasks.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NotBlankIfPresentValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotBlankIfPresent {

    String message() default "{validation.blankIfPresent}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

