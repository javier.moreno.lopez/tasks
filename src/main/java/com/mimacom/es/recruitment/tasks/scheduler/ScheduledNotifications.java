package com.mimacom.es.recruitment.tasks.scheduler;

import com.mimacom.es.recruitment.tasks.persistence.TaskRepository;
import com.mimacom.es.recruitment.tasks.persistence.UserRepository;
import com.mimacom.es.recruitment.tasks.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

/**
 * Scheduled task that search for all the non-finalized tasks that due sooner
 * than one day in the future for each user that has an email.
 */
@Component
public class ScheduledNotifications {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TaskRepository taskRepository;
	@Autowired
	private MailService mailService;

	@Scheduled(cron = "${scheduler.cron.notifications}")
	public void notifyUsers() {
		OffsetDateTime deadline = OffsetDateTime.now().plusDays(1);
		userRepository.findByEmailIsNotNull().forEach(user -> mailService.sendNotification(
				user,
				taskRepository.findAllByUserAndFinalizedIsFalseAndDeadlineLessThan(user, deadline)
		));
	}
}
