package com.mimacom.es.recruitment.tasks.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * Stores a user for authentication purposes. Username must have a maximum
 * length of 15 character and must contain only letters or numbers.
 */
@Entity
@Table(name = "users")
public class User implements Serializable {
	
	private static final long serialVersionUID = -6572078748160002820L;

	// Problems i18n:
	// https://stackoverflow.com/questions/58257635/message-parameters-not-resolved-in-localized-spring-boot-validation-messages
	@Id
	@Size(min = 1, max = 15, message = "validation.size.user.name")
	@Pattern(regexp = "^[a-zA-Z0-9]+$", message = "validation.pattern.user.name")
	private String username;

	@Size(min = 4, max = 255, message = "validation.size.user.password")
	@NotBlank
	private String password;

	@Pattern(regexp = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", message = "validation.pattern.user.email")
	@Size(min = 7, max = 255, message = "validation.size.user.email")
	private String email;

	@Size(min = 5, max = 5, message = "validation.size.user.locale")
	private String locale;

	@OneToMany(mappedBy = "user")
	private Set<Task> tasks;

	// For the sake of JPA
	protected User() {
		super();
	}

	public User(String username, String password, String email, String locale) {
		this();
		this.username = username;
		this.password = password;
		this.email = email;
		this.locale = locale;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String getLocale() {
		return locale;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return Objects.equals(username, user.username) && Objects.equals(password, user.password) && Objects.equals(email, user.email) && Objects.equals(locale, user.locale) && Objects.equals(tasks, user.tasks);
	}

	@Override
	public int hashCode() {
		return Objects.hash(username, password, email, locale, tasks);
	}

	@Override
	public String toString() {
		return "User [username=" + username + "]";
	}
}
