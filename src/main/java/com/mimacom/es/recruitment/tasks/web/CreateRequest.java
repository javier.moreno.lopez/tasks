package com.mimacom.es.recruitment.tasks.web;

import com.mimacom.es.recruitment.tasks.validation.NotBlankIfPresent;
import com.mimacom.es.recruitment.tasks.validation.groups.CreationProcess;
import com.mimacom.es.recruitment.tasks.validation.groups.UpdateProcess;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.OffsetDateTime;

public class CreateRequest implements Serializable {

    private static final long serialVersionUID = 1914424128088786561L;

    @Size(min = 1, max = 255, message = "validation.size.task.title", groups = CreationProcess.class)
    @NotBlank(groups = CreationProcess.class)
    @NotBlankIfPresent(groups = UpdateProcess.class)
    protected final String title;

    protected final OffsetDateTime deadline;

    public CreateRequest(String title, OffsetDateTime deadline) {
        this.title = title;
        this.deadline = deadline;
    }

    public String getTitle() {
        return title;
    }

    public OffsetDateTime getDeadline() {
        return deadline;
    }

    @Override
    public String toString() {
        return "CreateRequest{" +
                "title='" + title + '\'' +
                ", deadline=" + deadline +
                '}';
    }
}
