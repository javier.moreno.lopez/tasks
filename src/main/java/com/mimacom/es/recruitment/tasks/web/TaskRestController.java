package com.mimacom.es.recruitment.tasks.web;

import com.mimacom.es.recruitment.tasks.domain.Task;
import com.mimacom.es.recruitment.tasks.persistence.TaskRepository;
import com.mimacom.es.recruitment.tasks.services.UserService;
import com.mimacom.es.recruitment.tasks.validation.groups.CreationProcess;
import com.mimacom.es.recruitment.tasks.validation.groups.UpdateProcess;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller that exposes a Rest API with CRUD operations over a task list.
 */
@RestController
public class TaskRestController {

	public static final String DEADLINE_FIELD = "deadline";
	public static final String TITLE_FIELD = "title";

	private final TaskRepository taskRepository;
	private final UserService userService;

	public TaskRestController(TaskRepository taskRepository, UserService userService) {
		this.taskRepository = taskRepository;
		this.userService = userService;
	}

	/**
	 * Creates a new task marked as not finalized and with a unique id.
	 *
	 * @param request may contain the following fields:
	 *                <ul>
	 *                <li>'title' must not be empty.</li>
	 *                <li>'deadline' is optional. An offset date time</li>
	 *                </ul>
	 * @return the id of the task created or <code>400</code> http status code if title is
	 * blank.
	 */
	@PostMapping("/tasks")
	@ResponseStatus(HttpStatus.CREATED)
	public TaskResponse create(@RequestBody @Validated(CreationProcess.class) CreateRequest request) {
		return toDto(taskRepository.save(new Task(userService.getUser(), request.getTitle(), request.getDeadline())));
	}

	/**
	 * Edits a task. Body has to contain at least one parameter ('title', 'finalize'
	 * or 'deadline')
	 *
	 * @param request may contain the following parameters:
	 *                <ul>
	 *                <li>'title' must not be empty.</li>
	 *                <li>'finalize' if provided must be 'true' or 'false'
	 *                <li>'deadline' is optional. An offset date time</li>
	 *                </ul>
	 * @param id      indicates which task must be edited.
	 * @return the edited task or <code>400</code> http status code if any of the following situation occurs:
	 * <ul>
	 * <li>There's no body.</li>
	 * <li>'finalize' has not a correct value.</li>
	 * <li>'deadline' is not a right offset date time.</li>
	 * </ul>
	 * or <code>404</code> if there's no task with the provided id.
	 */
	@PutMapping("/tasks/{id}")
	@Transactional
	public TaskResponse edit(@PathVariable long id, @RequestBody @Validated(UpdateProcess.class) UpdateRequest request) {
		final Task task = taskRepository.findByIdAndUser(id, userService.getUser())
				.orElseThrow(() -> new EntityNotFoundException("Task not found. [id=" + id + "]"));

		if (StringUtils.hasText(request.getTitle())) {
			task.setTitle(request.getTitle());
		}
		if (request.getFinalized() != null) {
			task.setFinalized(Boolean.TRUE.equals(request.getFinalized()));
		}
		if (request.getDeadline() != null) {
			task.setDeadline(request.getDeadline());
		}

		return toDto(task);
	}

	@GetMapping("/tasks/{id}")
	public TaskResponse get(@PathVariable long id) {

		return toDto(taskRepository.findByIdAndUser(id, userService.getUser())
				.orElseThrow(() -> new EntityNotFoundException("Task not found. [id=" + id + "]")));
	}

	/**
	 * Returns all tasks not already finalized and sorted by <code>title</code>.
	 * Allows pagination.
	 *
	 * @param pageNo   number of page (defaults to 0). First page is 0.
	 * @param pageSize limit of number of tasks to return (defaults to 10).
	 * @param sortBy   may only be 'title' or 'deadline'
	 * @return all the tasks not already finalized.
	 */
	@GetMapping("/tasks")
	public List<TaskResponse> getAll(
			@RequestParam(defaultValue = "0") int pageNo,
			@RequestParam(defaultValue = "10") int pageSize,
			@RequestParam(defaultValue = TITLE_FIELD) String sortBy
	) {
		final Pageable paging = PageRequest.of(
				pageNo,
				pageSize,
				(DEADLINE_FIELD.equals(sortBy))
						? Sort.by(DEADLINE_FIELD)
						: Sort.by(TITLE_FIELD)
		);

		return toDto(taskRepository.findByFinalizedAndUser(false, userService.getUser(), paging));
	}

	/**
	 * @param id indicates the task to be deleted.
	 */
	@DeleteMapping("/tasks/{id}")
	@Transactional
	public void delete(@PathVariable long id) {
		try {
			taskRepository.deleteByIdAndUser(id, userService.getUser());
		} catch (EmptyResultDataAccessException e) {
			// NOP. REST delete should be idempotent
		}
	}

	/**
	 * Returns all tasks already finalized and sorted by <code>title</code>. Allows
	 * pagination.
	 *
	 * @param pageNo   number of page (defaults to 0). First page is 0.
	 * @param pageSize limit of number of tasks to return (defaults to 10).
	 * @return all the tasks already finalized.
	 */
	@GetMapping("/history")
	public List<TaskResponse> getHistory(
			@RequestParam(defaultValue = "0") int pageNo,
			@RequestParam(defaultValue = "10") int pageSize
	) {
		return toDto(taskRepository.findByFinalizedAndUser(
				true,
				userService.getUser(),
				PageRequest.of(pageNo, pageSize, Sort.by(TITLE_FIELD))
		));
	}

	private List<TaskResponse> toDto(final Page<Task> tasks) {
		return (tasks.hasContent())
				? tasks.getContent().stream().map(this::toDto).collect(Collectors.toList())
				: new ArrayList<>();
	}

	private TaskResponse toDto(final Task task) {
		return new TaskResponse(task.getId(), task.getTitle(), task.getDeadline(), task.isFinalized());
	}
}
