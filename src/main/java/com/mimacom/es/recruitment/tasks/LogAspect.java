package com.mimacom.es.recruitment.tasks;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Aspect that allows to log any invocation of a method: invocation parameters,
 * return value, exception thrown and execution time.
 */
@Aspect
@Component
public class LogAspect {
	@Around("execution(* com.mimacom..*(..))")
	public Object logCall(ProceedingJoinPoint proceedingJointPoint) throws Throwable {
		Signature signature = proceedingJointPoint.getStaticPart().getSignature();
		String methodFullyQualifiedName = String.format("%s.%s", signature.getDeclaringTypeName(), signature.getName());
		Logger logger = LoggerFactory.getLogger(methodFullyQualifiedName);
		if (!logger.isTraceEnabled())
			return proceedingJointPoint.proceed(proceedingJointPoint.getArgs());

		Object[] args = proceedingJointPoint.getArgs();
		String argList = Arrays.stream(args).map(Object::toString).collect(Collectors.joining(", "));
		String invocation = String.format("%s(%s)", methodFullyQualifiedName, argList);
		logger.trace(String.format("%s started.", invocation));
		long start = System.nanoTime();
		try {
			Object proceed = proceedingJointPoint.proceed(args);
			long end = System.nanoTime();
			logger.trace(String.format("%s ended after %f ms returning: %s.", invocation,
					((double) (end - start) / 1000000), proceed));
			return proceed;
		} catch (Throwable T) {
			long end = System.nanoTime();
			logger.trace(String.format("%s thrown exception after %f ms. %s.", invocation,
					((double) (end - start) / 1000000), T));
			throw T;
		}
	}
}
