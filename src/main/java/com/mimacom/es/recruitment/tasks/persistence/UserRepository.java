package com.mimacom.es.recruitment.tasks.persistence;

import com.mimacom.es.recruitment.tasks.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, String> {

	List<User> findByEmailIsNotNull();
}
