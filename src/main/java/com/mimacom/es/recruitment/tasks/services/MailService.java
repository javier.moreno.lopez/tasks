package com.mimacom.es.recruitment.tasks.services;

import com.mimacom.es.recruitment.tasks.domain.Task;
import com.mimacom.es.recruitment.tasks.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Sends a mail notification to a user with non-finalized tasks that has deadlines (ordered by deadlines).
 */
@Service
public class MailService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd - HH:mm:ss");

	public final JavaMailSender mailSender;
	public final MessageSource messageSource;

	public MailService(JavaMailSender mailSender, MessageSource messageSource) {
		this.mailSender = mailSender;
		this.messageSource = messageSource;
	}

	public void sendNotification(User user, List<Task> tasks) {
		if (user == null || tasks == null || tasks.isEmpty()) {
			return;
		}

		final Locale locale = (null != user.getLocale())
				? Locale.forLanguageTag(user.getLocale())
				: LocaleContextHolder.getLocale();
		String body = messageSource.getMessage("mail.body.header", new String[]{user.getUsername()}, locale);
		body += tasks.stream().sorted(Comparator.comparing(Task::getDeadline))
				.map(task -> messageSource.getMessage(
						"mail.body.item",
						new String[]{task.getTitle(), task.getDeadline().format(FORMATTER)},
						locale
				)).collect(Collectors.joining("\n"));

		final SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setTo(user.getEmail());
		simpleMailMessage.setText(body);
		simpleMailMessage.setSubject(messageSource.getMessage("mail.subject.notification", null, locale));
		
		try {
			mailSender.send(simpleMailMessage);
			LOGGER.info("Sent notification email. [recipient={}, task={}]", user.getUsername(), tasks.size());
		} catch (MailException ex) {
			LOGGER.error("Error sending mail.", ex);
		}
	}
}
