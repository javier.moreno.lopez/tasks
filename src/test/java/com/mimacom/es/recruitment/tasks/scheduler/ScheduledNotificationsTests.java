package com.mimacom.es.recruitment.tasks.scheduler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mimacom.es.recruitment.tasks.domain.Task;
import com.mimacom.es.recruitment.tasks.domain.User;
import com.mimacom.es.recruitment.tasks.persistence.TaskRepository;
import com.mimacom.es.recruitment.tasks.persistence.UserRepository;
import com.mimacom.es.recruitment.tasks.services.MailService;

/**
 * Unit tests for ScheduledNotifications.
 * 
 * @author Javier Moreno Lopez
 * @version 1.1 2020-05-28
 *
 */
@Tag("unit")
@ExtendWith(MockitoExtension.class)
class ScheduledNotificationsTests {
	@Mock
	private TaskRepository taskRepository;
	@Mock
	private UserRepository userRepository;
	@Mock
	private MailService mailService;

	@InjectMocks
	private ScheduledNotifications scheduledNotifications = new ScheduledNotifications();

	private static final User userTest = new User("Test", "Test", "prueba@prueba.com", "es_ES");

	@Test
	@DisplayName("Checking if ScheduledNotifications notify users correctly")
	void testMailServiceSendNotifications_Succeed() {
		List<Task> tasks = Collections
				.singletonList(new Task(userTest, "Test", OffsetDateTime.parse("2020-05-26T08:34:30Z")));
		when(userRepository.findByEmailIsNotNull()).thenReturn(Collections.singletonList(userTest));
		when(taskRepository.findAllByUserAndFinalizedIsFalseAndDeadlineLessThan(any(User.class),
				any(OffsetDateTime.class))).thenReturn(tasks);

		scheduledNotifications.notifyUsers();
		Mockito.verify(mailService, Mockito.times(1)).sendNotification(userTest, tasks);
	}

}
