package com.mimacom.es.recruitment.tasks.services;

import com.mimacom.es.recruitment.tasks.domain.Task;
import com.mimacom.es.recruitment.tasks.domain.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Tag("unit")
@ExtendWith(MockitoExtension.class)
class MailServiceTest {

	private static final User USER_TEST_TEST_DATA = new User("Test", "Test", "prueba@prueba.com", "es_ES");

	@Mock
	private JavaMailSender javaMailSender;
	@SuppressWarnings("unused")
	@Mock
	private MessageSource messageSource;

	@InjectMocks
	private MailService testee;

	@Test
	@DisplayName("Checking if MailService sends notifications")
	void sendNotification_whenInvoked_thenSuccess() {
		testee.sendNotification(USER_TEST_TEST_DATA,
				Collections.singletonList(new Task(USER_TEST_TEST_DATA, "Test", OffsetDateTime.parse("2020-05-26T08:34:30Z"))));
		Mockito.verify(javaMailSender, Mockito.times(1)).send(ArgumentMatchers.any(SimpleMailMessage.class));
	}

	@Test
	void sendNotification_whenNoUser_thenDoNothing() {
		final Task task = mock(Task.class);

		testee.sendNotification(null, Collections.singletonList(task));

		verify(javaMailSender, never()).send(any(SimpleMailMessage.class));
	}

	@ParameterizedTest
	@NullAndEmptySource
	void sendNotification_whenNoTasks_thenDoNothing(List<Task> tasks) {
		final User user = mock(User.class);
		testee.sendNotification(user, tasks);

		verify(javaMailSender, never()).send(any(SimpleMailMessage.class));
	}
}
