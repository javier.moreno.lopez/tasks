package com.mimacom.es.recruitment.tasks.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mimacom.es.recruitment.tasks.domain.Task;
import com.mimacom.es.recruitment.tasks.domain.User;
import com.mimacom.es.recruitment.tasks.persistence.TaskRepository;
import com.mimacom.es.recruitment.tasks.persistence.UserRepository;
import com.mimacom.es.recruitment.tasks.services.UserService;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

import javax.sql.DataSource;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SuppressWarnings("unchecked") // Not important for a test class
@Tag("unit")
@WebMvcTest
@WithMockUser
class TaskRestControllerTests {

	private static final User USER_ES_TEST_DATA = new User("user", "password", null, "es");
	private static final String HEADER_AUTHENTICATION_BAD_TEST_DATA = "Basic " + Base64Utils.encodeToString("non-existent-user:password".getBytes()); // to use with @WithMockUser

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private TaskRepository taskRepository;
	@MockBean
	private UserService userService;
	@SuppressWarnings("unused")
	@MockBean
	private UserRepository userRepository;
	@SuppressWarnings("unused")
	@MockBean
	private DataSource dataSource;

	@ParameterizedTest(name = "{index}: {1} ({0})")
	@MethodSource("getArgumentsToTestSecurity")
	@DisplayName("Checking authentication in all endpoints")
	void test_whenNotAuthorized_thenUnauthorized(HttpMethod httpMethod, String endpoint, Long id) throws Exception {
		mockMvc.perform(
				request(httpMethod, endpoint, id)
						.header(HttpHeaders.AUTHORIZATION, HEADER_AUTHENTICATION_BAD_TEST_DATA)
		).andExpect(status().isUnauthorized());
	}

	private static Stream<Arguments> getArgumentsToTestSecurity() {
		return Stream.of(
				arguments(HttpMethod.GET, "/tasks", null),
				arguments(HttpMethod.GET, "/tasks/{id}", 1L),
				arguments(HttpMethod.POST, "/tasks", null),
				arguments(HttpMethod.PUT, "/tasks/{id}", 1L),
				arguments(HttpMethod.DELETE, "/tasks/{id}", 1L),
				arguments(HttpMethod.GET, "/history", null)
		);
	}

	@Test
	@DisplayName("Checking if TaskRestController.create creates successfully a Task given a non-blank title")
	void testCreate_whenInvoked_thenSucceed() throws Exception {
		final String title = "Test";
		final long id = 1L;
		final CreateRequest request = new CreateRequest(title, null);
		final Task taskExpected = new Task(USER_ES_TEST_DATA, title, null);
		final TaskResponse expected = new TaskResponse(id, title, null, false);
		final Task taskStored = mock(Task.class);
		when(userService.getUser()).thenReturn(USER_ES_TEST_DATA);
		when(taskRepository.save(taskExpected)).thenReturn(taskStored);
		when(taskStored.getId()).thenReturn(id);
		when(taskStored.getTitle()).thenReturn(title);

		mockMvc.perform(
						post("/tasks")
								.accept(MediaType.APPLICATION_JSON)
								.contentType(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(request))
				)
				.andExpect(status().isCreated())
				.andExpect(content().json(objectMapper.writeValueAsString(expected)));
	}

	@Test
	@DisplayName("Checking if trying to create a Task with an empty title returns a bad request code")
	void testCreate_whenEmptyTitle_thenBadRequest() throws Exception {
		final String title = " ";
		final CreateRequest request = new CreateRequest(title, null);

		mockMvc.perform(
						post("/tasks")
								.accept(MediaType.APPLICATION_JSON)
								.contentType(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(request))
				)
				.andExpect(status().isBadRequest());
	}

	@Test
	@DisplayName("Checking if trying to create a Task with an empty title and Spanish language in header returns a bad request code")
	void testCreate_WhenEmptyTitleAndSpanishHeader_thenBadRequest() throws Exception {
		final String title = " ";
		final CreateRequest request = new CreateRequest(title, null);

		mockMvc.perform(
						post("/tasks")
								.header(HttpHeaders.ACCEPT_LANGUAGE, "es")
								.accept(MediaType.APPLICATION_JSON)
								.contentType(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(request))
				)
				.andExpect(status().isBadRequest())
				.andExpect((result) ->
						assertEquals(
								"'title' no debe estar vacío", // Provided by javax.validation.constraints.NotBlank.message key in NotBlank javax.validation.constrains annotation
								result.getResponse().getContentAsString(StandardCharsets.UTF_8)
						));
	}

	@Test
	@DisplayName("Checking if when creating a task with an empty payload returns a bad request code")
	void testCreate_whenNoPayload_thenBadRequest() throws Exception {
		mockMvc.perform(post("/tasks")).andExpect(status().isBadRequest());
	}

	@Test
	@DisplayName("Checking if TaskRestController.getAll paginates")
	void testGetAll_whenInvoked_thenSuccess() throws Exception {
		final int pageSize = 2;
		final Task task1 = mock(Task.class);
		final Task task2 = mock(Task.class);
		final List<Task> tasks = List.of(task1, task2);
		when(userService.getUser()).thenReturn(USER_ES_TEST_DATA);
		when(task1.getId()).thenReturn(1L);
		when(task2.getId()).thenReturn(2L);
		when(taskRepository.findByFinalizedAndUser(
				false,
				USER_ES_TEST_DATA,
				PageRequest.of(0, pageSize, Sort.by("title"))
		)).thenReturn(new PageImpl<>(tasks));

		final List<TaskResponse> result = objectMapper.readValue(
				mockMvc.perform(get("/tasks?pageSize={pagesize}", pageSize)
								.contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andReturn().getResponse().getContentAsString(),
				List.class
		);

		assertNotNull(result);
		assertEquals(pageSize, result.size());
	}

	@Test
	@DisplayName("Checking if TaskRestController.getHistory paginates")
	void testGetHistory_whenInvoked_ThenSuccess() throws Exception {
		final int pageSize = 2;
		final Task task1 = mock(Task.class);
		final Task task2 = mock(Task.class);
		final List<Task> tasks = List.of(task1, task2);
		when(userService.getUser()).thenReturn(USER_ES_TEST_DATA);
		when(task1.getId()).thenReturn(1L);
		when(task2.getId()).thenReturn(2L);
		when(taskRepository.findByFinalizedAndUser(
				true,
				USER_ES_TEST_DATA,
				PageRequest.of(0, pageSize, Sort.by("title"))
		)).thenReturn(new PageImpl<>(tasks));

		final List<TaskResponse> result = objectMapper.readValue(
				mockMvc.perform(get("/history?pageSize={pagesize}", pageSize)
								.contentType(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk())
						.andReturn().getResponse().getContentAsString(),
				List.class
		);

		assertNotNull(result);
		assertEquals(pageSize, result.size());
	}

	@Test
	@DisplayName("Checking if TaskRestController.edit edits successfully a Task")
	void testEdit_whenInvoked_thenSuccess() throws Exception {
		final String title = "Test";
		final long id = 1L;
		final UpdateRequest request = new UpdateRequest(title, null, true);
		final TaskResponse expected = new TaskResponse(id, title, null, true);
		final Task task = mock(Task.class);
		when(userService.getUser()).thenReturn(USER_ES_TEST_DATA);
		when(taskRepository.findByIdAndUser(id, USER_ES_TEST_DATA)).thenReturn(Optional.of(task));
		when(task.getId()).thenReturn(id);
		when(task.getTitle()).thenReturn(title);
		when(task.isFinalized()).thenReturn(true);

		mockMvc.perform(
						put("/tasks/{id}", id)
								.accept(MediaType.APPLICATION_JSON)
								.contentType(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(request))
				)
				.andExpect(status().isOk())
				.andExpect(content().json(objectMapper.writeValueAsString(expected)));

		final InOrder inOrder = inOrder(task, taskRepository);
		inOrder.verify(taskRepository).findByIdAndUser(id, USER_ES_TEST_DATA);
		inOrder.verify(task).setTitle(title);
		inOrder.verify(task).setFinalized(true);
	}

	@Test
	@DisplayName("Checking if when editing a title with an empty title returns a bad request code")
	void testEdit_whenEmptyTitle_thenBadRequest() throws Exception {
		final String title = " ";
		final long id = 1L;
		final UpdateRequest request = new UpdateRequest(title, null, false);

		mockMvc.perform(
						put("/tasks/{id}", id)
								.accept(MediaType.APPLICATION_JSON)
								.contentType(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(request))
				)
				.andExpect(status().isBadRequest());
	}

	@Test
	@DisplayName("Checking if when editing a title with an empty payload returns a bad request code")
	void testEdit_whenNoPayload_thenBadRequest() throws Exception {
		final long id = 1L;

		mockMvc.perform(put("/tasks/{id}", id)).andExpect(status().isBadRequest());
	}

	@Test
	@DisplayName("Checking if when editing a title with an empty title returns a bad request code for a Spanish user")
	void testEdit_whenEmptyTitleNoLanguageHeaderButSpanishStoredLanguage_thenBadRequest() throws Exception {
		final String title = " ";
		final long id = 1L;
		final UpdateRequest request = new UpdateRequest(title, null, false);
		when(userService.getUser()).thenReturn(USER_ES_TEST_DATA);

		mockMvc.perform(
						put("/tasks/{id}", id)
								.accept(MediaType.APPLICATION_JSON)
								.contentType(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(request))
				)
				.andExpect(status().isBadRequest())
				.andExpect((result) ->
						assertEquals(
								"'title' si se aporta no debe estar vacío",
								result.getResponse().getContentAsString(StandardCharsets.UTF_8)
						));
	}

	@Test
	@DisplayName("Checking if when editing a title with an empty title returns a bad request code for a Spanish user with different language header")
	void testEdit_whenEmptyTitleLanguageHeaderEnglishButSpanishStoredLanguage_thenBadRequest() throws Exception {
		final String title = " ";
		final long id = 1L;
		final UpdateRequest request = new UpdateRequest(title, null, false);
		when(userService.getUser()).thenReturn(USER_ES_TEST_DATA);

		mockMvc.perform(
						put("/tasks/{id}", id)
								.header(HttpHeaders.ACCEPT_LANGUAGE, "en")
								.accept(MediaType.APPLICATION_JSON)
								.contentType(MediaType.APPLICATION_JSON)
								.content(objectMapper.writeValueAsString(request))
				)
				.andExpect(status().isBadRequest())
				.andExpect((result) ->
						assertEquals(
								"'title' if present must not be blank",
								result.getResponse().getContentAsString(StandardCharsets.UTF_8)
						));
	}
}