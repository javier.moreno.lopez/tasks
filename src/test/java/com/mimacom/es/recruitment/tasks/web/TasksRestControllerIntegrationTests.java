package com.mimacom.es.recruitment.tasks.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mimacom.es.recruitment.tasks.domain.Task;
import com.mimacom.es.recruitment.tasks.domain.User;
import com.mimacom.es.recruitment.tasks.persistence.TaskRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("ConstantConditions") // If NPE occurs tests will fail
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Tag("integration")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:controllers/beforeTasksRestControllerIntegrationTestsRun.sql")
@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:controllers/afterTasksRestControllerIntegrationTestsRun.sql")
class TasksRestControllerIntegrationTests {
	@LocalServerPort
	private int port;

	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired
	private TaskRepository taskRepository;

	private final static User user = new User("Test",
			"{bcrypt}$2a$10$NEe.FLndlogipFyk0Sbx..OTLc.W8cCezHAywJKCvObQEHhxsGo4K", null, null);

	@ParameterizedTest
	@CsvSource(value = {
			"Test, Test, , ,false, 3, ",
			"Test2, Test, , ,false, 1, ",
			"Test, Test, , 2, false, 2, ", // Pagination
			"Test, Test, 1, 2, false, 1, ", // Pagination
			"TestDeadline, Test, , 1, false, 1, Test 1", // Sorting
			"TestDeadline, Test, , 1, true, 1, Test 2", // Sorting
			"Test, Test,2, , false, 0, " // Empty page
	})
	@DisplayName("Checking if a GET request to 'tasks' endpoint with proper credentials for different users is success")
	void testGetTasks_givenAuthorizedRequest_shouldSucceed(
			String username,
			String password,
			String pageNo,
			String pageSize,
			boolean sortByDeadline,
			int expectedNumberOfTasksReturned,
			String expectedFirstTitle
	) {
		final ResponseEntity<List<Task>> response = restTemplate.withBasicAuth(username, password).exchange(
				getUriBuilder("tasks", pageNo, pageSize, sortByDeadline).build().toString(),
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<>() {
				}
		);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(expectedNumberOfTasksReturned, response.getBody().size());
		if (expectedFirstTitle != null) {
			assertEquals(expectedFirstTitle, response.getBody().get(0).getTitle());
		}
	}

	@ParameterizedTest
	@CsvSource(value = {
			"Test, Test, , , 2",
			"Test2, Test, , , 1",
			"Test, Test, 0, 1,  1", // Pagination
			"Test, Test, 1, 1,  1", // Pagination
			"Test, Test, 1, ,  0" // Empty page
	})
	@DisplayName("Checking if a GET request to 'history' endpoint with proper credentials for different users is success")
	void testGetHistory_givenAuthorizedRequest_shouldSucceed(
			String username,
			String password,
			String pageNo,
			String pageSize,
			int expectedNumberOfTasksReturned
	) {
		final ResponseEntity<List<Task>> response = restTemplate.withBasicAuth(username, password).exchange(
				getUriBuilder("history", pageNo, pageSize, false).build().toString(),
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<>() {
				}
		);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(expectedNumberOfTasksReturned, response.getBody().size());
	}

	private UriBuilder getUriBuilder(
			final String endpoint,
			final String pageNo,
			final String pageSize,
			final boolean sortByDeadline
	) {
		final UriBuilder uri = UriComponentsBuilder.newInstance()
				.scheme("http")
				.host("localhost")
				.port(port)
				.path("/" + endpoint);
		if (pageNo != null) {
			uri.queryParam("pageNo", pageNo);
		}
		if (pageSize != null) {
			uri.queryParam("pageSize", pageSize);
		}
		if (sortByDeadline) {
			uri.queryParam("sortBy", "deadline");
		}

		return uri;
	}

	@Test
	@DisplayName("Checking if a POST request to 'tasks' endpoint with proper credentials succeeds")
	void testPostTasks_whenInvoked_thenSuccess() throws Exception {
		ResponseEntity<Task> response = restTemplate.withBasicAuth("Test", "Test").postForEntity(
				"http://localhost:" + port + "/tasks", generateRequest("test", null, OffsetDateTime.of(2020, 5, 26, 9, 34, 40, 0, ZoneOffset.of("+01:00"))),
				Task.class);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals("test", response.getBody().getTitle());
		assertFalse(response.getBody().isFinalized());
		assertEquals(OffsetDateTime.parse("2020-05-26T08:34:40Z"), response.getBody().getDeadline());

		taskRepository.delete(response.getBody());
	}

	@Test
	@DisplayName("Checking if a PUT request to 'tasks' endpoint with proper credentials succeeds")
	void testPutTasks_whenInvoked_thenSuccess() throws Exception {
		Task task = taskRepository.save(new Task(user, "Test"));

		ResponseEntity<Task> response = restTemplate.withBasicAuth("Test", "Test").exchange(
				"http://localhost:" + port + "/tasks/" + task.getId(), HttpMethod.PUT,
				generateRequest("Test edited", true, OffsetDateTime.of(2020, 5, 26, 9, 34, 40, 0, ZoneOffset.of("+01:00"))), Task.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals("Test edited", response.getBody().getTitle());
		assertTrue(response.getBody().isFinalized());
		assertEquals(OffsetDateTime.parse("2020-05-26T08:34:40Z"), response.getBody().getDeadline());

		taskRepository.delete(response.getBody());
	}

	@Test
	@DisplayName("Checking if a PUT request to 'tasks' endpoint with proper credentials on a non-existent task return 'Bad request' HTPP status")
	void testPutTasks_whenNonExistentTask_thenNotFound() throws Exception {
		ResponseEntity<String> response = restTemplate.withBasicAuth("Test", "Test").exchange(
				"http://localhost:" + port + "/tasks/0", HttpMethod.PUT, generateRequest("test", true, null),
				String.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}


	@Test
	@DisplayName("Checking if a DELETE request to 'tasks' endpoint with proper credentials succeeds")
	void testDeleteTasks_whenInvoked_thenSuccess() throws Exception {
		Task task = taskRepository.save(new Task(user, "Test"));

		assertNotNull(taskRepository.findById(task.getId()).orElse(null));

		ResponseEntity<String> response = restTemplate.withBasicAuth("Test", "Test").exchange(
				"http://localhost:" + port + "/tasks/" + task.getId(), HttpMethod.DELETE,
				generateRequest(null, null, null), String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertNull(taskRepository.findById(task.getId()).orElse(null));
	}

	private HttpEntity<String> generateRequest(
			String title,
			Boolean finalize,
			OffsetDateTime deadline
	) throws JsonProcessingException {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return new HttpEntity<>(objectMapper.writeValueAsString(new UpdateRequest(title, deadline, finalize)), headers);
	}
}
