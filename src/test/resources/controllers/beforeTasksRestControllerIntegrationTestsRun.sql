INSERT INTO users (username, password) VALUES ('Test', '{bcrypt}$2a$10$NEe.FLndlogipFyk0Sbx..OTLc.W8cCezHAywJKCvObQEHhxsGo4K');
INSERT INTO users (username, password) VALUES ('Test2', '{bcrypt}$2a$10$NEe.FLndlogipFyk0Sbx..OTLc.W8cCezHAywJKCvObQEHhxsGo4K');
INSERT INTO users (username, password) VALUES ('TestDeadline', '{bcrypt}$2a$10$NEe.FLndlogipFyk0Sbx..OTLc.W8cCezHAywJKCvObQEHhxsGo4K');
INSERT INTO users (username, password, locale) VALUES ('Test locale es', '{bcrypt}$2a$10$NEe.FLndlogipFyk0Sbx..OTLc.W8cCezHAywJKCvObQEHhxsGo4K', 'es-ES');
INSERT INTO users (username, password, locale) VALUES ('Test locale en', '{bcrypt}$2a$10$NEe.FLndlogipFyk0Sbx..OTLc.W8cCezHAywJKCvObQEHhxsGo4K', 'en');

INSERT INTO tasks (user_username, id, title, finalized) VALUES ('Test', 201, 'Test 1', FALSE);
INSERT INTO tasks (user_username, id, title, finalized) VALUES ('Test', 202, 'Test 2', FALSE);
INSERT INTO tasks (user_username, id, title, finalized) VALUES ('Test', 203, 'Test 3', FALSE);
INSERT INTO tasks (user_username, id, title, finalized) VALUES ('Test', 204, 'Test 1 finalized', TRUE);
INSERT INTO tasks (user_username, id, title, finalized) VALUES ('Test', 205, 'Test 2 finalized', TRUE);

INSERT INTO tasks (user_username, id, title, finalized) VALUES ('Test2', 206, 'Test 1', FALSE);
INSERT INTO tasks (user_username, id, title, finalized) VALUES ('Test2', 207, 'Test 1 finalized', TRUE);

INSERT INTO tasks (user_username, id, title, finalized, deadline) VALUES ('TestDeadline', 208, 'Test 1', FALSE, '2020-05-27 11:22:05');
INSERT INTO tasks (user_username, id, title, finalized, deadline) VALUES ('TestDeadline', 209, 'Test 2', FALSE, '2020-05-26 11:22:05');